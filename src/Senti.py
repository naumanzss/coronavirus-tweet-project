import time
import re
import sys
from textblob import TextBlob
from vaderSentiment.vaderSentiment import SentimentIntensityAnalyzer

import nltk
from nltk.corpus import stopwords
# nltk.download('stopwords')
# nltk.download('wordnet')
from nltk.stem.wordnet import WordNetLemmatizer
import string

from textblob.classifiers import NaiveBayesClassifier
import pandas as pd

data = []
output = []
stop = set(stopwords.words('english')) 
exclude = set(string.punctuation) 
lemma = WordNetLemmatizer()

negative = ['not', 'neither', 'nor', 'but', 'however', 'although', 'nonetheless', 'despite', 'except',
                         'even though', 'yet']
# output[0] is text
#       [1] is coordinates
#       [2] is polarity 


def read_json():
    global data
    f = open(sys.argv[1], "r") 

    raw_data = f.read().strip().split('\n')
    index = 0

    for a in raw_data : 
        data.append(eval(a))
        # index +=1 
        # if(index ==  10):
        #     break

    f.close() 

def clean(temp):
    stop_free = " ".join([i for i in temp.lower().split() if i not in stop if i not in negative])
    punc_free = "".join([ch for ch in stop_free if ch not in exclude])
    normalized = " ".join([lemma.lemmatize(word) for word in punc_free.split()])
    return normalized


def sentiment_check1():
    global output
    pattern = re.compile(',')
    space_re = re.compile(r'\s+')
    for tweet in data:
        temp_actual = TextBlob(tweet['text'].replace("\n" , " "))
        temp = clean(temp_actual)
        temp = TextBlob(temp)
        # print(temp)
        if(temp.sentiment.polarity > 0):
            emotion = "Positive"
        elif(temp.sentiment.polarity == 0):
            emotion = "Neutral"
        elif(temp.sentiment.polarity < 0):
            emotion = "Negative"
        
        try:
            place_full_name = pattern.sub('', tweet['place']['full_name'])
        except:
            place_full_name = None
        
        try:
            ts = time.strftime('%Y-%m-%d %H:%M:%S', time.strptime(tweet['created_at'],'%a %b %d %H:%M:%S +0000 %Y'))
        except:
            ts = None

        try:
            place_coordinates = tweet['coordinates']['coordinates']
        except:
            place_coordinates = None

        if(place_coordinates is None):
            try:
                place_coordinates = tweet['place']['bounding_box']['coordinates'][0][0]
            except:
                place_coordinates = None

        if place_coordinates is not None and emotion is not "Neutral":  
            place_coordinates = space_re.sub('', str(place_coordinates)[1:-1])
            output.append([ place_full_name, place_coordinates , ts, emotion, temp.sentiment.polarity + 1 ])

    # for lines in output:
    #     print(lines)
    #     print()

    out = open("Sentimental_CSVs/emo_" + sys.argv[1][:-5] + ".csv", "w")

    for lines in output:
        for value in lines:

            out.write(str(value) + "," )

        out.write("\n")

    out.close()

def sentiment_check2():
    
    analyzer = SentimentIntensityAnalyzer()

    for tweet in data:
        # temp = TextBlob(tweet['text'])
        print(tweet['text'])
        print(analyzer.polarity_scores(tweet['text']))


if __name__ == "__main__":
    read_json()
    sentiment_check1()
    # sentiment_check2()
