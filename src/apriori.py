#!/usr/bin/env python3
from collections import defaultdict
from argparse import ArgumentParser
from pprint import pprint
from itertools import combinations
from sys import argv
from efficient_apriori import apriori
import re

word_baskets = []
common = set(['to', 'of', 'in', 'for', 'on', 'with', 'at', 'by', 'from', 'up', 'about', 'into', 'over', 'after', 'the', 'and', 'a', 'that', 'I', 'it', 'not', 'he', 'as', 'you', 'this', 'but', 'his','they','her','she','or','an','will','my','one','all','would','there','their', 'coronavirus', 'covid19'])

def get_tweet_text(tweet):
    tweet_object = eval(tweet)
    tweet_text = tweet_object['text']
    words = [re.sub('[^0-9a-zA-Z]', '', x) for x in tweet_text.lower().split()]
    words = [x for x in words if x is not '']
    return words

def read_file(filename):
    with open(filename) as infile:
        for tweet in infile:
            words = set(get_tweet_text(tweet)) - common
            word_baskets.append(tuple(words))

if __name__ == '__main__':

    # Designate input and output file
    filename = argv[1]
    output = argv[2]
    # Set Support Threshold
    support = float(argv[3])
    confidence = float(argv[4])

    read_file(filename)

    itemsets, rules = apriori(word_baskets, min_support=support, min_confidence=confidence, verbosity=1)

    # open output file for writing
    f = open(output, 'w')

    for rule in sorted(rules, key=lambda rule: rule.lift):
        f.write(str(rule) + "\n")

    f.close()

