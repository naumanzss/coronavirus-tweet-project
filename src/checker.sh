#!/bin/bash

# Cronjob to check to make sure twitter scraper is running. 
# If job has failed, restart.

FILEPATH=/home/cyphy1/coronavirus-tweet-project

PROCESS=$(pgrep -f scraper)
echo $(date) >> $FILEPATH/cron.log;
if [[ -z $PROCESS ]] ; then 
        echo "Process was terminated early: Restarting." >> $FILEPATH/cron.log; 
        python3 $FILEPATH/scripts/scraper.py >> $FILEPATH/tweet_data/$(date +\%Y\%m\%d).json 2>>$FILEPATH/cron.log & disown
else
	echo "Process was running, no action taken." >> $FILEPATH/cron.log;
fi


