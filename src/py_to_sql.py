#!/usr/bin/env python3

import mysql.connector

cnx = mysql.connector.connect(user='cyphy1', password='csci6627',
        host='localhost', database='covid19')

cursor = cnx.cursor()

add_tweet = ("INSERT INTO tweet 
        (created_at, id, text, truncated, in_reply_to_status_id, 
        in_reply_to_user_id, user_id, lng, lat, place_id, is_quote_status, 
        quote_count, reply_count, retweet_count, favorite_count, favorited, retweeted)
        VALUES (%s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s)")

add_user = ("INSERT INTO user
        (user_id, verified, followers_count, friends_count, listed_count, 
        favourites_count, statuses_count, created_at, geo_enabled)
        VALUES (%s %s %s %s %s %s %s %s %s)")

add_place = ("INSERT INTO place
        (place_id, full_name, country, country_code, place_type, avg_lat, avg_long) 
        VALUES (%s %s %s %s %s %s %s)")

cnx.close()

