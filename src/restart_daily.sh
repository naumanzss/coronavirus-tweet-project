#!/bin/bash

# Cronjob to kill and restart scraper at midnight daily.

LOGPATH=/home/cyphy1/coronavirus-tweet-project
SCRIPTPATH=/home/cyphy1/coronavirus-tweet-project/scripts
DATAPATH=/home/cyphy1/coronavirus-tweet-project/tweet_data
PID=$(pgrep -f scraper)

if [[ -n $PID ]]; then
	kill -9 $PID
fi

touch $DATAPATH/$(date +\%Y\%m\%d).json
chown cyphy1:cyphy1 $DATAPATH/$(date +\%Y\%m\%d).json
python3 $SCRIPTPATH/scraper.py >> $DATAPATH/$(date +\%Y\%m\%d).json 2>>$LOGPATH/cron.log & disown


