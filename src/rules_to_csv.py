#!/usr/bin/env python3

import re
import csv
from argparse import ArgumentParser

def strip_text(text):
    return re.sub('[^0-9a-zA-Z\._ ]', '', text)

def parse_attributes(attrs):
    p = re.compile(r'\d+\.\d+')  # Compile a pattern to capture float values
    floats = [i for i in p.findall(attrs)]  # Convert strings to float
    return floats

if __name__ == '__main__':

    parser = ArgumentParser(description = 'Write apriori rules from input file into csv output file.')
    parser.add_argument('infile', help='path to input file')

    args = parser.parse_args()

    out = open("%s.csv" % strip_text(args.infile), 'w')
    out.write("set1,set2,size1,size2,confidence,support,lift,conviction\n")
    with open(args.infile, 'r') as f:
        for line in f:
            split = [x for x in re.split('{|}', line) if x is not '']
            out.write(strip_text(split[0])  # itemset 1
                    + "," + strip_text(split[2])  # itemset 2
                    + "," + str(len(split[0].split())) # count of itemset 1
                    + "," + str(len(split[2].split())) # count of itemset 2
                    + ',' + ','.join(parse_attributes(split[3])) # supp, conf, lift, conv
                    +  '\n')
    f.close()
