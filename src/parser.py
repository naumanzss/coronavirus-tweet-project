#!/usr/bin/env python3

import multiprocessing
import string
from time import time
from collections import defaultdict
import re
import json

filename = './20200316.json'

def process_chunk(line):
    # Convert text to python object
    counts = defaultdict(int)
    tweet = eval(line)
    # parse uniq words from tweet text after converting to lowercase and removing punctuation.
    uniq = set(tweet['text'].lower().translate(str.maketrans('', '', string.punctuation)).split())
    for word in uniq:
        counts[word] += 1
    for 
    


#for x in open(filename, 'r'):
#    print(process_chunk(x))

if __name__ == '__main__':
    start = time()
    pool = multiprocessing.Pool(6)
    lines = []
    with open(filename, 'r') as infile:
        lines += pool.map(process_chunk, infile, 1000)
    end = time()
    print("Process finished in %f seconds." % (end - start))
    print(len(lines))

