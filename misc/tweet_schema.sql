CREATE TABLE `tweet` (
  `created_at` datetime,
  `id` bigint,
  `text` text,
  `truncated` tinyint,
  `in_reply_to_status_id` bigint DEFAULT null,
  `in_reply_to_user_id` bigint DEFAULT null,
  `user_id` bigint,
  `lng` double DEFAULT null,
  `lat` double DEFAULT null,
  `place_id` varchar(20) DEFAULT null,
  `is_quote_status` tinyint,
  `quote_count` integer,
  `reply_count` integer,
  `retweet_count` integer,
  `favorite_count` integer,
  `favorited` tinyint,
  `retweeted` tinyint
);

CREATE TABLE `user` (
  `user_id` bigint PRIMARY KEY,
  `verified` tinyint,
  `followers_count` bigint,
  `friends_count` bigint,
  `listed_count` bigint,
  `favourites_count` bigint,
  `statuses_count` bigint,
  `created_at` datetime,
  `geo_enabled` tinyint
);

CREATE TABLE `place` (
  `place_id` varchar(20) PRIMARY KEY,
  `full_name` varchar(50),
  `country` varchar(30),
  `country_code` char(3),
  `place_type` varchar(20),
  `avg_lat` double,
  `avg_long` double
);

ALTER TABLE `tweet` ADD FOREIGN KEY (`place_id`) REFERENCES `place` (`place_id`);

ALTER TABLE `tweet` ADD FOREIGN KEY (`user_id`) REFERENCES `user` (`user_id`);
